import { readFile } from "./common-functions.js";

let tmp = readFile('inputs/day9.txt');

// split all elements of tmp into a list of numbers
let valueHistories = tmp.map(line => line.split(' ').map(Number));


// Helper functions ///////////////////////////////////////////////////////////////

function getDerivativeOfArray(numbers) {
    let diff = [];
    for (let i = 1; i < numbers.length; i++) {
        diff.push(numbers[i] - numbers[i - 1]);
    }
    return diff;
}

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let extrapolatedValues = [];

valueHistories.forEach(valueHistory => {
    let diffTree = [valueHistory];
    diffTree.push(getDerivativeOfArray(valueHistory));

    //while not all elements of diff are 0
    while (diffTree[diffTree.length - 1].some(element => element != 0)) {
        // recalculate diff
        diffTree.push(getDerivativeOfArray(diffTree[diffTree.length - 1]));
    }

    // diff is now an array of 0s - we add another one to the right
    diffTree[diffTree.length - 1].push(0);

    // now generate the new values along the tree
    for (let i = diffTree.length-2; i >= 0; i--) {
        diffTree[i].push(diffTree[i+1].slice(-1)[0] + diffTree[i].slice(-1)[0]);
    }

    extrapolatedValues.push(diffTree[0].slice(-1)[0]);
});

// log the sum of all extrapolated values
console.log("Part 1: Sum of all extrapolated values: " + extrapolatedValues.reduce((a, b) => a + b));

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

extrapolatedValues = [];

valueHistories.forEach(valueHistory => {
    let diffTree = [valueHistory];
    diffTree.push(getDerivativeOfArray(valueHistory));

    //while not all elements of diff are 0
    while (diffTree[diffTree.length - 1].some(element => element != 0)) {
        // recalculate diff
        diffTree.push(getDerivativeOfArray(diffTree[diffTree.length - 1]));
    }

    // diff is now an array of 0s - we add another one to the left
    diffTree[diffTree.length - 1].unshift(0);

    // now generate the new values along the tree
    for (let i = diffTree.length-2; i >= 0; i--) {
        diffTree[i].unshift(diffTree[i][0] - diffTree[i+1][0]);
    }

    extrapolatedValues.push(diffTree[0][0]);
});

// log the sum of all extrapolated values
console.log("Part 2: Sum of all extrapolated values: " + extrapolatedValues.reduce((a, b) => a + b));

//#endregion

