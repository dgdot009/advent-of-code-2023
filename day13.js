import { readFile, sumArray } from "./common-functions.js";

let input = readFile('inputs/day13.txt');
let patterns = input.join('\n').split('\n\n');

//#region Helper functions ///////////////////////////////////////////////////////////////

function findVerticalReflection(patternRows) {
    // find first vertical reflection
    for (let i = 1; i < patternRows.length; i++) {
        for (let j = i; j >= 0; j--) {
            if (j == 0) {
                return i; // i is already a 1-based row here
            }
            if (i+j-1 >= patternRows.length) {
                continue;
            }
            if (patternRows[i+j-1] != patternRows[i-j]) {
                break;
            }
        }
    }
    return 0;
}

function findAllVerticalReflections(patternRows) {
    let reflections = [];
    for (let i = 1; i < patternRows.length; i++) {
        for (let j = i; j >= 0; j--) {
            if (j == 0) {
                reflections.push(i); // i is already a 1-based row here
                break;
            }
            if (i+j-1 >= patternRows.length) {
                continue;
            }
            if (patternRows[i+j-1] != patternRows[i-j]) {
                break;
            }
        }
    }
    return reflections;
}

function findReflectionsAfterFix(patternRows) {
    let originalReflection = findVerticalReflection(patternRows);

    for (let c = 0; c < patternRows[0].length; c++) {
        for (let r = 0; r < patternRows.length; r++) {
            fixSmudge(patternRows, r, c);
            let potentialReflections = findAllVerticalReflections(patternRows)
            fixSmudge(patternRows, r, c);

            for (let i = 0; i < potentialReflections.length; i++) {
                if (potentialReflections[i] > 0 && potentialReflections[i] != originalReflection) {
                    return potentialReflections[i];
                    }
            }
        }
    }
    return 0;
}

function fixSmudge(patternRows, row, column) {
    if (patternRows[row][column] == '#') {
        patternRows[row] = patternRows[row].substring(0, column) + '.' + patternRows[row].substring(column + 1);
    }
    else {
        patternRows[row] = patternRows[row].substring(0, column) + '#' + patternRows[row].substring(column + 1);
    }
    return patternRows;
}

/**
 * Transposes a string pattern by swapping rows and columns.
 *
 * @param {string} pattern - The pattern to transpose.
 * @returns {string} The transposed pattern.
 */
function transpose(pattern) {
    let patternRows = pattern.split('\n');
    let patternColumns = [];

    for (let j = 0; j < patternRows[0].length; j++) {
        let patternColumn = '';
        for (let i = 0; i < patternRows.length; i++) {
            patternColumn += patternRows[i][j];
        }
        patternColumns.push(patternColumn);
    }

    return patternColumns.join('\n');
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let reflectionRows = [];
let reflectionColumns = [];

patterns.forEach(pattern => {
    reflectionRows.push(findVerticalReflection(pattern.split('\n')));
    reflectionColumns.push(findVerticalReflection(transpose(pattern).split('\n')));
})

let summarizedNotes =  sumArray(reflectionColumns) + 100 * sumArray(reflectionRows);

console.log("Part 1: The sum of all notes is: " + summarizedNotes);
// 28651

//#endregion


//#region Part 2 /////////////////////////////////////////////////////////////////////////

let newReflectionRows = [];
let newReflectionColumns = [];

patterns.forEach(pattern => {
    let patternRows = pattern.split('\n');
    newReflectionRows.push(findReflectionsAfterFix(patternRows));

    let patternColumns = transpose(pattern).split('\n');
    newReflectionColumns.push(findReflectionsAfterFix(patternColumns));
})

summarizedNotes =  sumArray(newReflectionColumns) + 100 * sumArray(newReflectionRows);

console.log("Part 2: The new sum of all notes is: " + summarizedNotes);
// 25450

//#endregion
