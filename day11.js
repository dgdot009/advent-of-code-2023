import { readFile } from "./common-functions.js";

let skyImage = readFile('inputs/day11.txt');

//#region Helper functions ///////////////////////////////////////////////////////////////

function extendUniverse(skyImage) {

    // duplicate empty columns
    for (let i = 0; i < skyImage[0].length; i++) {
        let count = 0;
        for (let j = 0; j < skyImage.length; j++) {
            if (skyImage[j][i] == '#') {
                count++;
            }
        }
        if (count == 0) {
            // insert a column of '.' into the array skyImage at position i
            for (let j = 0; j < skyImage.length; j++) {
                skyImage[j] = skyImage[j].slice(0, i) + '.' + skyImage[j].slice(i);
            }
            i++;
        }
    }

    // duplicate empty rows
    for (let i = 0; i < skyImage.length; i++) {
        // check if line contains any galaxy
        if (!skyImage[i].includes('#')) {
            // insert a row of '.' into the array skyImage at position i
            skyImage.splice(i, 0, '.'.repeat(skyImage[i].length));
            i++;
        }
    }
}

function getExpansionRowsCols(skyImage) {
    let expansionRows = [];
    let expansionCols = [];

    // find empty columns
    for (let i = 0; i < skyImage[0].length; i++) {
        let count = 0;
        for (let j = 0; j < skyImage.length; j++) {
            if (skyImage[j][i] == '#') {
                count++;
            }
        }
        if (count == 0) {
            expansionCols.push(i);
        }
    }

    // find empty rows
    for (let i = 0; i < skyImage.length; i++) {
        // check if line contains any galaxy
        if (!skyImage[i].includes('#')) {
            expansionRows.push(i);
        }
    }

    return [expansionRows, expansionCols];
}

function getNumberOfCrossedExpansions(expansionRows, expansionCols, galaxy1, galaxy2) {
    let crossedExpansions = 0;

    // find the number of values between x and y in array expansionRows
    let rowCoords = [galaxy1[0], galaxy2[0]].sort()
    let colCoords = [galaxy1[1], galaxy2[1]].sort()

    rowCoords.sort((a, b) => a - b);
    colCoords.sort((a, b) => a - b);

    for (let i = 0; i < expansionRows.length; i++) {
        if (expansionRows[i] > rowCoords[0] && expansionRows[i] < rowCoords[1]) {
            crossedExpansions++;
        }
    }
    for (let i = 0; i < expansionCols.length; i++) {
        if (expansionCols[i] > colCoords[0] && expansionCols[i] < colCoords[1]) {
            crossedExpansions++;
        }
    }

    return crossedExpansions;
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

// start with extending the universe
extendUniverse(skyImage);

// In the skyImage, find the coordinates of all galaxies '#'
let galaxyCoordinates = [];
for (let i = 0; i < skyImage.length; i++) {
    for (let j = 0; j < skyImage[0].length; j++) {
        if (skyImage[i][j] == '#') {
            galaxyCoordinates.push([i, j]);
        }
    }
}

let sumOfMinDistances = 0;

// for each galaxy, calculate the distance to all other galaxies
for (let i = 0; i < galaxyCoordinates.length; i++) {
    for (let j = i + 1; j < galaxyCoordinates.length; j++) {
        sumOfMinDistances += Math.abs(galaxyCoordinates[i][0] - galaxyCoordinates[j][0]) + Math.abs(galaxyCoordinates[i][1] - galaxyCoordinates[j][1]);
    }
}

console.log("Part 1: The sum of all closest distances is: " + sumOfMinDistances);

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

// read original map again
skyImage = readFile('inputs/day11.txt');

let expansionRows, expansionCols;
[expansionRows, expansionCols] = getExpansionRowsCols(skyImage);

// In the skyImage, find the coordinates of all galaxies '#'
galaxyCoordinates = [];
for (let i = 0; i < skyImage.length; i++) {
    for (let j = 0; j < skyImage[0].length; j++) {
        if (skyImage[i][j] == '#') {
            galaxyCoordinates.push([i, j]);
        }
    }
}

sumOfMinDistances = 0;
let crossedExpansions = 0;
const expansionFactor = 1e6;
// const expansionFactor = 100;

// For each galaxy, calculate the distance to all other galaxies
for (let i = 0; i < galaxyCoordinates.length; i++) {
    for (let j = i + 1; j < galaxyCoordinates.length; j++) {
        crossedExpansions = getNumberOfCrossedExpansions(expansionRows, expansionCols, galaxyCoordinates[i], galaxyCoordinates[j])
        sumOfMinDistances += crossedExpansions * expansionFactor - crossedExpansions;
        sumOfMinDistances += Math.abs(galaxyCoordinates[i][0] - galaxyCoordinates[j][0]) + Math.abs(galaxyCoordinates[i][1] - galaxyCoordinates[j][1]);
    }
}

console.log("Part 2: The sum of all closest distances is: " + sumOfMinDistances);

//#endregion
