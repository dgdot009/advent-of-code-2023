import { readFile, cloneArray } from "./common-functions.js";

let inputMap = readFile('inputs/day21.txt');

//#region Helper functions ///////////////////////////////////////////////////////////////

function step(mapState, emptyMap)
{
    let newlySteppendOn = [];
    for (let i = 0; i < mapState.length; i++) {
        for (let j = 0; j < mapState[0].length; j++) {
            if (mapState[i][j] == 2) {
                if (i > 0 && mapState[i - 1][j] == 0) {
                    newlySteppendOn.push([i - 1, j]);
                }
                if (i < mapState.length - 1 && mapState[i + 1][j] == 0) {
                    newlySteppendOn.push([i + 1, j]);
                }
                if (j > 0 && mapState[i][j - 1] == 0) {
                    newlySteppendOn.push([i, j - 1]);
                }
                if (j < mapState[0].length - 1 && mapState[i][j + 1] == 0) {
                    newlySteppendOn.push([i, j + 1]);
                }
            }
        }
    }

    for (let i = 0; i < mapState.length; i++) {
        for (let j = 0; j < mapState[0].length; j++) {
            mapState[i][j] = emptyMap[i][j];
        }
    }

    for (let i = 0; i < newlySteppendOn.length; i++) {
        mapState[newlySteppendOn[i][0]][newlySteppendOn[i][1]] = 2;
    }
    return mapState;
}

function countSteppedOn(mapState)
{
    let steppedOn = 0;
    for (let i = 0; i < mapState.length; i++) {
        for (let j = 0; j < mapState[0].length; j++) {
            if (mapState[i][j] == 2) {
                steppedOn++;
            }
        }
    }
    return steppedOn;
}

function getDerivativeOfArray(numbers) {
    let diff = [];
    for (let i = 1; i < numbers.length; i++) {
        diff.push(numbers[i] - numbers[i - 1]);
    }
    return diff;
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

var startingPosition = [0, 0];

// parse the map
let gardenMap = Array(inputMap.length).fill(0).map(() => Array(inputMap[0].length).fill(0));
for (let i = 0; i < inputMap.length; i++) {
    for (let j = 0; j < inputMap[0].length; j++) {
        if (inputMap[i][j] == '#') {
            gardenMap[i][j] = 1;
        }
        else if (inputMap[i][j] == 'S') {
            startingPosition = [i, j];
            gardenMap[i][j] = 2;
        }
    }
}

let emptyGardenMap = JSON.parse(JSON.stringify(gardenMap));
emptyGardenMap[startingPosition[0]][startingPosition[1]] = 0;

for (let i = 0; i < 64; i++) {
    gardenMap = step(gardenMap, emptyGardenMap);
}

// count the number of "2" in the map
let steppedOn = countSteppedOn(gardenMap);

console.log("Part 1: Total number of plots stepped on: " + steppedOn);
// 3776

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

// parse the map
gardenMap = Array(inputMap.length).fill(0).map(() => Array(inputMap[0].length).fill(0));
for (let i = 0; i < inputMap.length; i++) {
    for (let j = 0; j < inputMap[0].length; j++) {
        if (inputMap[i][j] == '#') {
            gardenMap[i][j] = 1;
        }
    }
}

let initialGardenSize = gardenMap.length;

// create enlarged map with repetitions
let numOfRepetitions = 3;

for (let i = 0; i< inputMap.length; i++) {
    for (let r = 0; r < numOfRepetitions; r++) {
        gardenMap[i] = gardenMap[i].concat(gardenMap[i].slice());
    }
}
for (let r = 0; r < numOfRepetitions; r++) {
    gardenMap = gardenMap.concat(cloneArray(gardenMap));
}

startingPosition = [Math.floor(initialGardenSize * (0.5 + (2 ** numOfRepetitions)/2)), Math.floor(initialGardenSize * (0.5 + (2 ** numOfRepetitions)/2))];

// put a 2 in the middle of the map for the starting point
gardenMap[startingPosition[0]][startingPosition[1]] = 2;

emptyGardenMap = cloneArray(gardenMap);
emptyGardenMap[startingPosition[0]][startingPosition[1]] = 0;

let stepedOnHistory = [countSteppedOn(gardenMap)];

for (let i = 0; i < 65 + 131 * 3 + 1; i++) {
    gardenMap = step(gardenMap, emptyGardenMap);
    stepedOnHistory.push(countSteppedOn(gardenMap));
}

// get values at 65 + 131*n for n = 0, 1, 2, ...
let diffTree = [[]];
for (let n = 0; 65 + 131 * n < stepedOnHistory.length; n++) {
    diffTree[0].push(stepedOnHistory[65 + 131 * n]);
}

// while not all elements of diff are 0
while (diffTree[diffTree.length - 1].some(element => element != 0)) {
    diffTree.push(getDerivativeOfArray(diffTree[diffTree.length - 1]));
}

// Using Gauss sum formula
let targetCycle = (26501365 - 65) / 131;
let stepCount = 30572*0.5*(targetCycle-3)*(targetCycle-2) + diffTree[0].at(-1) + 1*(targetCycle-3) * diffTree[1].at(-1);

// Explicit calculation
// let targetCycle = (26501365 - 65) / 131;
// let stepCount = diffTree[0][diffTree[0].length - 1];
// let nextStep = diffTree[1][diffTree[1].length - 1];

// for (let i = diffTree[1].length; i < targetCycle; i++) {
//     nextStep += diffTree[2][0];
//     stepCount += nextStep;
// }

console.log("Part 2: Total number of plots stepped on: " + stepCount);
// 625587097150084

//#endregion
