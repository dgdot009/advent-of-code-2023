import { readFile, sumArray } from "./common-functions.js";

let digPlan = readFile('inputs/day18.txt');

let digPlanArray = digPlan.map(line => line.split(' '));
digPlanArray = digPlanArray[0].map((col, i) => digPlanArray.map(row => row[i]));

//#region Helper functions ///////////////////////////////////////////////////////////////

function findEnclosedRegion(bitMap) {

    let regionMap = Array(bitMap.length).fill(0).map(() => Array(bitMap[0].length).fill(0));

    let insideFlag = false;
    let potentialCrossingUp = false;
    let potentialCrossingDown = false;
    let insideCount = 0;

    for (let i = 1; i < bitMap.length - 1; i++) {
        insideFlag = false;
        potentialCrossingUp = false;
        potentialCrossingDown = false;
        for (let j = 1; j < bitMap[0].length; j++) {

            if (bitMap[i][j] > 0) {
                // check for direct crossing
                if (Math.abs(bitMap[i][j] - bitMap[i][j+1]) == 1 && Math.abs(bitMap[i][j] - bitMap[i][j-1]) == 1) {
                    insideFlag = !insideFlag;
                    potentialCrossingUp = false;
                    potentialCrossingDown = false;
                    continue;
                }

                if (potentialCrossingDown)
                {
                    if (Math.abs(bitMap[i][j] - bitMap[i+1][j]) == 0) {
                        insideFlag = !insideFlag;
                        potentialCrossingDown = false;
                    }
                    else if (Math.abs(bitMap[i][j] - bitMap[i-1][j]) == 0) {
                        potentialCrossingDown = false;
                    }
                }
                else if (potentialCrossingUp)
                {
                    if (Math.abs(bitMap[i][j] - bitMap[i-1][j]) == 0) {
                        insideFlag = !insideFlag;
                        potentialCrossingUp = false;
                    }
                    else if (Math.abs(bitMap[i][j] - bitMap[i+1][j]) == 0) {
                        potentialCrossingUp = false;
                    }
                }
                else {
                    // check for potential future crossing along the row
                    if (Math.abs(bitMap[i][j] - bitMap[i-1][j]) == 1) {
                        potentialCrossingUp = true;
                    }
                    else if (Math.abs(bitMap[i][j] - bitMap[i+1][j]) == 1) {
                        potentialCrossingDown = true;
                    }
                }
            }
            else {
                potentialCrossingUp = false;
                potentialCrossingDown = false;
                if (insideFlag) {
                    regionMap[i][j] = 1;
                    insideCount++;
                }
            }
        }
    }
    return [regionMap, insideCount];
}

// this function takes two points (x y) and finds all intersections with a line given by the equation y = mx + b
function getLineIntersections(x1, y1, x2, y2, m, b) {
    let x = 0;
    let y = 0;

    // check for vertical line
    if (x1 == x2) {
        x = x1;
        y = m * x + b;
        if (y < Math.min(y1, y2) || y > Math.max(y1, y2)) {
            return [];
        }
        return [x, y];
    }

    // check for horizontal line
    if (y1 == y2) {
        y = y1;
        x = (y - b) / m;
        if (x < Math.min(x1, x2) || x > Math.max(x1, x2)) {
            return [];
        }
        return [x, y];
    }

    // check for parallel line
    if (m == 0) {
        return [];
    }

    // check for perpendicular line
    if (m == Infinity) {
        x = x1;
        y = m * x + b;
        return [x, y];
    }

    // calculate intersection
    x = (y1 - b) / m;
    y = m * x + b;
    if (x < Math.min(x1, x2) || x > Math.max(x1, x2)) {
        return [];
    }
    if (y < Math.min(y1, y2) || y > Math.max(y1, y2)) {
        return [];
    }
    return [x, y];
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

// get width and height of the dig plan
let digAreaWidth = 2;
let digAreaHeight = 2;
for (let i = 0; i < digPlanArray[0].length; i++) {
    if (digPlanArray[0][i] == "U" || digPlanArray[0][i] == "D") {
        digAreaHeight += Number(digPlanArray[1][i]);
    }
    else if (digPlanArray[0][i] == "L" || digPlanArray[0][i] == "R") {
        digAreaWidth += Number(digPlanArray[1][i]);
    }
}

//create a 2D array of the dig area
let digArea = Array(digAreaHeight).fill(0).map(() => Array(digAreaWidth).fill(0));

// start digging in the middle of the dig area
let currentDigLocation = [Math.round(digAreaHeight / 2), Math.round(digAreaWidth / 2)];
digArea[currentDigLocation[0]][currentDigLocation[1]] = 1;

for (let i = 0; i < digPlanArray[0].length; i++) {
    if (digPlanArray[0][i] == "U") {
        for (let j = 0; j < Number(digPlanArray[1][i]); j++) {
            currentDigLocation[0]--;
            digArea[currentDigLocation[0]][currentDigLocation[1]] = 1;
        }
    }
    else if (digPlanArray[0][i] == "D") {
        for (let j = 0; j < Number(digPlanArray[1][i]); j++) {
            currentDigLocation[0]++;
            digArea[currentDigLocation[0]][currentDigLocation[1]] = 1;
        }
    }
    else if (digPlanArray[0][i] == "L") {
        for (let j = 0; j < Number(digPlanArray[1][i]); j++) {
            currentDigLocation[1]--;
            digArea[currentDigLocation[0]][currentDigLocation[1]] = 1;
        }
    }
    else if (digPlanArray[0][i] == "R") {
        for (let j = 0; j < Number(digPlanArray[1][i]); j++) {
            currentDigLocation[1]++;
            digArea[currentDigLocation[0]][currentDigLocation[1]] = 1;
        }
    }
}

let [_, insideCount] = findEnclosedRegion(digArea)

console.log("Part 1: Cubic meters of lava that can be stored: " + (insideCount + sumArray(digArea)));
// 70253

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

currentDigLocation = [0, 0];
let digCorners = [];
let totalDigLength = 2;

for (let i = 0; i < digPlanArray[0].length; i++) {
    digCorners.push(currentDigLocation.slice());
    let digLength = parseInt(digPlanArray[2][i].slice(2, -2), 16);
    totalDigLength += digLength;

    // directions: 0 = right, 1 = down, 2 = left, 3 = up
    let digDirection = digPlanArray[2][i].slice(-2, -1);

    if (digDirection == "0") {
        currentDigLocation[0]+= digLength;
    }
    else if (digDirection == "1") {
        currentDigLocation[1]-= digLength;
    }
    else if (digDirection == "2") {
        currentDigLocation[0]-= digLength;
    }
    else if (digDirection == "3") {
        currentDigLocation[1]+= digLength;
    }
}

digArea = -(digCorners[digCorners.length-1][0] - digCorners[0][0]) * (digCorners[digCorners.length-1][1] + digCorners[0][1]);

for (let i = 0; i < digCorners.length - 1; i++) {
    digArea -= (digCorners[i][0] - digCorners[i+1][0]) * (digCorners[i][1] + digCorners[i+1][1]);
}
digArea /= 2;
digArea += totalDigLength / 2

console.log("Part 2: Cubic meters of lava that can be stored: " + digArea);
// 131265059885080

//#endregion
