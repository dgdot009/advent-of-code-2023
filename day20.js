import { readFile } from "./common-functions.js";

let moduleConfiguration = readFile('inputs/day20.txt');

//#region Helper functions ///////////////////////////////////////////////////////////////

const ModuleType = {   // enum for module types
    FLIPFLOP: 0,
    CONJUNCTION: 1,
    BROADCAST: 2,
};

class Module {
    constructor(destinationModules, name) {
        this.destinationModules = destinationModules;
        this.sourceModules = [];
        this.name = name.replace('&', '').replace('%', '');
        if (name[0] == '%') {
            this.type = ModuleType.FLIPFLOP;

        }
        else if (name[0] == '&') {
            this.type = ModuleType.CONJUNCTION;
        }
        else {
            this.type = ModuleType.BROADCAST;
        }
    }

    initStates() {
        if (this.type == ModuleType.CONJUNCTION) {
            // Add dictionary for input states
            this.inputStates = {};
            for (let module of this.sourceModules) {
                this.inputStates[module.name] = false;
            }
        }
        else if (this.type == ModuleType.FLIPFLOP) {
            this.state = false;
        }
    }

    processPulse(level, sourceModule) {

        let outputPulses = [];

        switch (this.type) {   // process the pulse based on module type
            case ModuleType.FLIPFLOP:
                if (!level) {
                    this.state = !this.state;
                    outputPulses = outputPulses.concat(this.sendPulse(this.state));
                }
                break;
            case ModuleType.CONJUNCTION:
                this.inputStates[sourceModule.name] = level;
                // check if all inputStates are high
                var allHigh = true;
                for (let key in this.inputStates) {
                    if (!this.inputStates[key]) {
                        allHigh = false;
                        break;
                    }
                }
                if (allHigh) {
                    outputPulses = outputPulses.concat(this.sendPulse(false));
                }
                else {
                    outputPulses = outputPulses.concat(this.sendPulse(true));
                }
                break;
            case ModuleType.BROADCAST:
                outputPulses = outputPulses.concat(this.sendPulse(level));
                break;
        }

        return outputPulses;
    }

    sendPulse(level) {
        let outputPulses = [];
        for (let module of this.destinationModules) {
            outputPulses.push(new Pulse(level, module, this));
        }
        return outputPulses;
    }

    getValueForState() {
        if (this.type == ModuleType.CONJUNCTION) {
            const binaryArray = Object.values(this.inputStates).map(state => state ? 1 : 0); // Convert input states to binary array
            const decimalValue = binaryArray.reduce((a, bit, i, arr) => a + (bit ? Math.pow(2, arr.length - i - 1) : 0), 0);
            return decimalValue; // Output: the decimal value of the input states
        }
    }}

class Pulse {
    constructor(level, destinationModule, sourceModule) {
        this.level = level;
        this.destinationModule = destinationModule;
        this.sourceModule = sourceModule;
    }

    // create string representation of pulse
    toString() {
        return this.sourceModule.name + ' -' + (this.level ? 'high' : 'low') + '-> ' + this.destinationModule.name;
    }
}

function lcmTwoNumbers(x, y) {
    if ((typeof x !== 'number') || (typeof y !== 'number'))
        return false;
    return (!x || !y) ? 0 : Math.abs((x * y) / gcdTwoNumbers(x, y));
}

function gcdTwoNumbers(x, y) {
    x = Math.abs(x);
    y = Math.abs(y);
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}

// create a function that finds the smallest common multiple of an array of numbers
function lcmArray(input) {
    if (toString.call(input) !== "[object Array]")
        return false;
    var r1 = input[0],
        i = 1;
    while (i < input.length) {
        r1 = lcmTwoNumbers(input[i], r1);
        i++;
    }
    return r1;
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

// parse the module configuration
let modules = {};

for (let line of moduleConfiguration) {
    let [source, destination] = line.split('->');
    let sourceModule = source.trim();
    let destinationModules = destination.split(',').map(module => module.trim());
    let module = new Module(destinationModules, sourceModule);
    // replace & and % in name
    sourceModule = sourceModule.replace('&', '');
    sourceModule = sourceModule.replace('%', '');
    modules[sourceModule] = module;
}

for (let line of moduleConfiguration) {
    let [source, destination] = line.split('->');
    let sourceModule = source.trim();
    sourceModule = sourceModule.replace('&', '');
    sourceModule = sourceModule.replace('%', '');
    let destinationModules = destination.split(',').map(module => module.trim());
    for (let module of destinationModules) {
        if (module in modules) {
            modules[module].sourceModules.push(modules[sourceModule]);
        }
        else {
            let newModule = new Module([], module);
            modules[module] = newModule;
            newModule.sourceModules.push(modules[sourceModule]);
        }
    }
    modules[sourceModule].destinationModules = destinationModules.map(module => modules[module]);
}

for (let key in modules) {
    modules[key].initStates();
}

let pulseLowCount = 0;
let pulseHighCount = 0;

for (let i = 0; i < 1000; i++) {
    let pulses = [];
    pulses = pulses.concat(modules['broadcaster'].sendPulse(false));
    pulseLowCount++;

    while (pulses.length > 0) {
        let pulse = pulses.shift();
        if (pulse.level) {
            pulseHighCount++;
        }
        else {
            pulseLowCount++;
        }
        pulses = pulses.concat(pulse.destinationModule.processPulse(pulse.level, pulse.sourceModule));
    }
}


console.log("Part 1: The value you get multiplying low and high pulses is: " + pulseHighCount*pulseLowCount);
// 787056720

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

for (let key in modules) {
    modules[key].initStates();
}

let buttonCount = 0;
let tmp = { 'sn': [], 'tf': [], 'lr': [], 'hl': [] };

while (true) {
    let pulses = [];
    buttonCount++;
    pulses = pulses.concat(modules['broadcaster'].sendPulse(false));
    pulseLowCount++;

    while (pulses.length > 0) {
        let pulse = pulses.shift();

        if (pulse.level == false && pulse.sourceModule.name == 'sn') {
            tmp['sn'].push(buttonCount);
        }
        if (pulse.level == false && pulse.sourceModule.name == 'tf') {
            tmp['tf'].push(buttonCount);
        }
        if (pulse.level == false && pulse.sourceModule.name == 'lr') {
            tmp['lr'].push(buttonCount);
        }
        if (pulse.level == false && pulse.sourceModule.name == 'hl') {
            tmp['hl'].push(buttonCount);
        }

        pulses = pulses.concat(pulse.destinationModule.processPulse(pulse.level, pulse.sourceModule));
    }
    if (buttonCount%10000 == 0) {
        break;
    }
}

let sn = tmp['sn'];
let diff_sn = sn.map((value, index) => value - sn[index - 1]).filter(value => value != 0);

let tf = tmp['tf'];
let diff_tf = tf.map((value, index) => value - tf[index - 1]).filter(value => value != 0);

let lr = tmp['lr'];
let diff_lr = lr.map((value, index) => value - lr[index - 1]).filter(value => value != 0);

let hl = tmp['hl'];
let diff_hl = hl.map((value, index) => value - hl[index - 1]).filter(value => value != 0);

// Takes the second element of all arrays and calculate the smallest common multiple
let lcm = lcmArray([diff_sn[1], diff_tf[1], diff_lr[1], diff_hl[1]]);

console.log("Part 2: The minimum number of button presses is:" + lcm);
// 212986464842911

//#endregion
