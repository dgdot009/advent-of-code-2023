import { readFile} from "./common-functions.js";

let sequence = readFile('inputs/day15.txt');
let commands = sequence[0].split(',');

//#region Helper functions ///////////////////////////////////////////////////////////////

function calcHash(cmd) {
    let hash = 0;
    for (let i = 0; i < cmd.length; i++) {
        // get ascii code of char
        hash += cmd.charCodeAt(i);
        hash = (hash * 17) % 256;
    }
    return hash;
}

function addLens(box, lens, focalLen) {
    for (let i = 0; i < box.length; i++) {
        if (box[i][0] == lens) {
            box[i] = [lens, focalLen]
            return;
        }
    }
    box.push([lens, focalLen]);
}

function removeLens(box, lens) {
    for (let i = 0; i < box.length; i++) {
        if (box[i][0] == lens) {
            box.splice(i, 1);
            return;
        }
    }
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let hashSum = 0;

commands.forEach(cmd => {
    hashSum += calcHash(cmd);
});

console.log("Part 1: The sum of all command hashes is: " + hashSum);
// 517315

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

// create empty arrays for boxes
let boxes = [];
for (let i = 0; i < 256; i++) {
    boxes.push([]);
}

commands.forEach(cmd => {
    if (cmd.indexOf('=') != -1) {
        let [lens, focalLen] = cmd.split('=');
        let boxId = calcHash(lens);
        addLens(boxes[boxId], lens, focalLen);
    }
    else {
        let lens = cmd.split('-')[0];
        let boxId = calcHash(lens);
        removeLens(boxes[boxId], lens);
    }
});

// calculate focusing power
let focusingPower = 0;
for (let i = 0; i < boxes.length; i++) {
    for (let j = 0; j < boxes[i].length; j++) {
        focusingPower += (i+1) * (j + 1) * parseInt(boxes[i][j][1]);
    }
}

console.log("Part 2: The total focusing power of the configuration is: " + focusingPower);
// 247763

//#endregion
