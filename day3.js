import { readFile } from "./common-functions.js";

let input = readFile('inputs/day3.txt');

// Helper functions //////////////////////////////////////////////

function convertSchematicToArray(schematic) {
    let schematicArray = new Array(schematic.length);

    for (let i = 0; i < schematic.length; i++) {
        schematicArray[i] = new Array(schematic[i].length + 2).fill(-1);
        for (let j = 0; j < schematic[0].length; j++) {
             if (schematic[i].charCodeAt(j) >= 48 && schematic[i].charCodeAt(j) <= 57) {
                schematicArray[i][j+1] = schematic[i].charCodeAt(j) - 48;
            }
            else if (schematic[i][j] == '*') {
                schematicArray[i][j+1] = -3;
            }
            else {
                schematicArray[i][j+1] = -2;
            }
        }
    }

    // add empty (-1) buffer rows to avoid out of bounds in search later
    schematicArray.unshift(new Array(schematicArray[0].length).fill(-1));
    schematicArray.push(new Array(schematicArray[0].length).fill(-1));

    return schematicArray;
}

function findAnySymbolAround(schematicArray, i, j) {
    // for all 8 directions around i, j, check the value in schematicArray
    if (schematicArray[i-1][j-1] <= -2 || schematicArray[i-1][j] <= -2 || schematicArray[i-1][j+1] <= -2 ||
        schematicArray[i][j-1] <= -2 || schematicArray[i][j+1] <= -2 ||
        schematicArray[i+1][j-1] <= -2 || schematicArray[i+1][j] <= -2 || schematicArray[i+1][j+1] <= -2) {
        return true;
    }
    return false;
}

function findPartsAround(schematicArray, i, j) {
    // for all 8 directions around i, j, check the value in schematicArray

    let partsAround = [];

    // check top and bottom middle entries first to avoid duplicated parts
    if (schematicArray[i-1][j] >= 0) {
        partsAround.push([i-1, j]);
    }
    else {
        if (schematicArray[i-1][j-1] >= 0) {
            partsAround.push([i-1, j-1]);
        }
        if (schematicArray[i-1][j+1] >= 0) {
            partsAround.push([i-1, j+1]);
        }
    }

    if (schematicArray[i][j-1] >= 0) {
        partsAround.push([i, j-1]);
    }
    if (schematicArray[i][j+1] >= 0) {
        partsAround.push([i, j+1]);
    }

    if (schematicArray[i+1][j] >= 0) {
        partsAround.push([i+1, j]);
    }
    else {
        if (schematicArray[i+1][j-1] >= 0) {
            partsAround.push([i+1, j-1]);
        }
        if (schematicArray[i+1][j+1] >= 0) {
            partsAround.push([i+1, j+1]);
        }
    }

    return partsAround;
}

function findCompletePartNumber(schematicArray, i, j) {
    let startIdx = j, endIdx = j;

    // find startIdx
    while (schematicArray[i][startIdx-1] >= 0) {
        startIdx--;
    }

    // find endIdx
    while (schematicArray[i][endIdx] >= 0) {
        endIdx++;
    }

    let digits = schematicArray[i].slice(startIdx, endIdx);
    return [parseInt(digits.join('')), endIdx];
}

// Part 1 //////////////////////////////////////////////

let sumOfAllParts = 0;

// convert input to numerical array
let schematicArray = convertSchematicToArray(input);

for (let i = 0; i < schematicArray.length; i++) {
    for (let j = 0; j < schematicArray[0].length; j++) {
        if (schematicArray[i][j] >= 0) {
            if (findAnySymbolAround(schematicArray, i, j)) {
                let [partNumber, endIdx] = findCompletePartNumber(schematicArray, i, j);
                j = endIdx;
                sumOfAllParts += partNumber;
            }
        }
    }
}

console.log('Part1 - Total sum of all Parts: ' + sumOfAllParts);

// Part 2 //////////////////////////////////////////////

let sumOfAllGearRatios = 0;

for (let i = 0; i < schematicArray.length; i++) {
    for (let j = 0; j < schematicArray[0].length; j++) {
        if (schematicArray[i][j] == -3) {
            let partsAround = findPartsAround(schematicArray, i, j);

            if (partsAround.length == 2) {
                let [partNumber1, ] = findCompletePartNumber(schematicArray, ...partsAround[0]);
                let [partNumber2, ] = findCompletePartNumber(schematicArray, ...partsAround[1]);
                sumOfAllGearRatios += partNumber1 * partNumber2;
            }
        }
    }
}

console.log('Part2 - Total sum of all gear ratios: ' + sumOfAllGearRatios);

