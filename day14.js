import { readFile } from "./common-functions.js";

let rockMapString = readFile('inputs/day14.txt');

//#region Helper functions ///////////////////////////////////////////////////////////////

function convertRockMapToValues(rockMap) {
    let convertedRockMap = [];
    for (let i = 0; i < rockMap.length; i++) {
        let rockMapRow = [];
        for (let j = 0; j < rockMap[i].length; j++) {
            if (rockMap[i][j] == '.') {
                rockMapRow.push(0);
            } else if (rockMap[i][j] == 'O') {
                rockMapRow.push(1);
            } else if (rockMap[i][j] == '#') {
                rockMapRow.push(2);
            }
            else {
                // raise error
                reportError("Unknown character in rockMap: " + rockMap[i][j]);
            }
        }
        convertedRockMap.push(rockMapRow);
    }
    return convertedRockMap;
}

function simulateRockSettling(rockMapColumn) {
    rockSettled = false;
    while (!rockSettled) {
        rockSettled = true;
        for (let i = 1; i < rockMapColumn.length; i++) {
            if (rockMapColumn[i] == 1) {
                if (rockMapColumn[i-1] == 0) {
                    rockMapColumn[i-1] = 1;
                    rockMapColumn[i] = 0;
                    rockSettled = false;
                }
            }
        }
    }
}

function calculateBeamLoad(rockMap) {
    let load = 0;

    for (let i = 0; i < rockMap.length; i++) {
        for (let j = 0; j < rockMap[i].length; j++) {
            if (rockMap[i][j] == 1) {
                load += rockMap.length - j;
            }
        }
    }
    return load;
}

function displayRockMap(rockMap) {
    let origRockMap = transposeArray(deepCopyArray(rockMap));

    let rockMapString = '';
    for (let i = 0; i < origRockMap.length; i++) {
        for (let j = 0; j < origRockMap[i].length; j++) {
            if (origRockMap[i][j] == 0) {
                rockMapString += '.';
            } else if (origRockMap[i][j] == 1) {
                rockMapString += 'O';
            } else if (origRockMap[i][j] == 2) {
                rockMapString += '#';
            }
            else {
                // raise error
                reportError("Unknown character in rockMap: " + origRockMap[i][j]);
            }
        }
        rockMapString += '\n';
    }
    console.log(rockMapString);
}

function deepCopyArray(array) {
    return JSON.parse(JSON.stringify(array));
}

function rotateArrayClockwise(matrix) {
    const n = matrix.length;
    const x = Math.floor(n/ 2);
    const y = n - 1;
    let k;
    for (let i = 0; i < x; i++) {
       for (let j = i; j < y - i; j++) {
          k = matrix[i][j];
          matrix[i][j] = matrix[y - j][i];
          matrix[y - j][i] = matrix[y - i][y - j];
          matrix[y - i][y - j] = matrix[j][y - i]
          matrix[j][y - i] = k
       }
    }
    return matrix;
}

function rotateArrayCounterClockwise(matrix) {
    const n = matrix.length;
    const x = Math.floor(n/ 2);
    const y = n - 1;
    let k;
    for (let i = 0; i < x; i++) {
         for (let j = i; j < y - i; j++) {
            k = matrix[i][j];
            matrix[i][j] = matrix[j][y - i];
            matrix[j][y - i] = matrix[y - i][y - j];
            matrix[y - i][y - j] = matrix[y - j][i]
            matrix[y - j][i] = k
            }
    }
    return matrix;
}

function transposeArray(matrix) {
    const n = matrix.length;
    let k;
    for (let i = 1; i < n; i++) {
       for (let j = 0; j < i; j++) {
          k = matrix[i][j];
          matrix[i][j] = matrix[j][i];
          matrix[j][i] = k;
       }
    }
    return matrix;
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let rockMap = convertRockMapToValues(rockMapString);

let rockSettled = false;

// let the rocks move!
while (!rockSettled) {
    rockSettled = true;
    for (let i = 1; i < rockMap.length; i++) {
        for (let j = 0; j < rockMap[i].length; j++) {
            if (rockMap[i][j] == 1) {
                if (rockMap[i-1][j] == 0) {
                    rockMap[i-1][j] = 1;
                    rockMap[i][j] = 0;
                    rockSettled = false;
                }
            }
        }
    }
}

// calculate the weight
let load = 0;
for (let i = 0; i < rockMap.length; i++) {
    for (let j = 0; j < rockMap[i].length; j++) {
        if (rockMap[i][j] == 1) {
            load += rockMap.length - i;
        }
    }
}

console.log("Part 1: The total load on the support beam is: " + load);
// 108641

//#endregion


//#region Part 2 /////////////////////////////////////////////////////////////////////////

rockMap = convertRockMapToValues(rockMapString);
rockMap = transposeArray(rockMap);

// create dictionary for stored rock movements
let rockSettlingDict = {};

// create dictionary for stored rock maps and their according load in a list
let rockMapDict = {};
let loadHistory = [0];

for (let cycle = 1; cycle <= 1000000000; cycle++) {

    for (let r = 0; r < 4; r++) {

        for (let i = 0; i < rockMap.length; i++) {

            if (rockMap[i].join('') in rockSettlingDict) {
                rockMap[i] = rockSettlingDict[rockMap[i].join('')].slice(0);
                continue;
            }
            else {
                let key = rockMap[i].join('');
                simulateRockSettling(rockMap[i]);
                rockSettlingDict[key] = rockMap[i].slice(0);
            }
        }
        rotateArrayCounterClockwise(rockMap);
    }

    if (rockMap.join('') in rockMapDict) {
        // we found a loop
        let lastSeen = rockMapDict[rockMap.join('')];
        let loopLength = cycle - lastSeen;
        let remainingCycles = 1000000000 - cycle;
        let cyclesToSkip = remainingCycles % loopLength;
        load = loadHistory[lastSeen + cyclesToSkip];
        break;
    }
    else {
        rockMapDict[rockMap.join('')] = cycle;
        loadHistory.push(calculateBeamLoad(rockMap));
    }
}

console.log("Part 2: The total load after 1000000000 cycles is: " + load);
// 84328

//#endregion
