import { readFile } from "./common-functions.js";

let input = readFile('inputs/day2.txt');

// Part 1 //////////////////////////////////////////////

let regExGreen = new RegExp('([0-9]*) *green', 'g');
let regExBlue = new RegExp('([0-9]*) *blue', 'g');
let regExRed = new RegExp('([0-9]*) *red', 'g');

let maxRed = 12;
let maxGreen = 13;
let maxBlue = 14;

let gameSum = 0;

// iterate over all lines in array input
for (let i = 0; i < input.length; i++) {
    let line = input[i];
    let gameId = line.split(':')[0];
    gameId = parseInt(gameId.split(' ')[1]);
    let gameSets = line.split(':')[1].split(';');

    let gamePossible = true;

    gameSets.forEach(element => {
        if (element.includes('green')) {
            regExGreen.lastIndex = 0;
            if (regExGreen.exec(element)[1] > maxGreen) {
                gamePossible = false;
                return;
            }
        }
        if (element.includes('blue')) {
            regExBlue.lastIndex = 0;
            if (regExBlue.exec(element)[1] > maxBlue) {
                gamePossible = false;
                return;
            }
        }
        if (element.includes('red')) {
            regExRed.lastIndex = 0;
            if (regExRed.exec(element)[1] > maxRed) {
                gamePossible = false;
                return;
            }
        }
    });

    if (gamePossible) {
        gameSum += gameId;
    }
}

console.log('Total sum in part 1: ' + gameSum);


// Part 2 //////////////////////////////////////////////

let gameSum2 = 0;

// iterate over all lines in array input
for (let i = 0; i < input.length; i++) {
    let line = input[i];
    let gameId = line.split(':')[0];
    gameId = parseInt(gameId.split(' ')[1]);
    let gameSets = line.split(':')[1].split(';');

    let minRed = 0, minGreen = 0, minBlue = 0;

    gameSets.forEach(element => {
        if (element.includes('red')) {
            regExRed.lastIndex = 0;
            let currentRed = parseInt(regExRed.exec(element)[1]);
            minRed = currentRed > minRed ? currentRed : minRed;
        }
        if (element.includes('green')) {
            regExGreen.lastIndex = 0;
            let currentGreen = parseInt(regExGreen.exec(element)[1]);
            minGreen = currentGreen > minGreen ? currentGreen : minGreen;
        }
        if (element.includes('blue')) {
            regExBlue.lastIndex = 0;
            let currentBlue = parseInt(regExBlue.exec(element)[1]);
            minBlue = currentBlue > minBlue ? currentBlue : minBlue;
        }
    });

    gameSum2 += minRed * minGreen * minBlue;
}


console.log('Total sum in part 2: ' + gameSum2);

