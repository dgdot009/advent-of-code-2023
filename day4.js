import { readFile, sumArray } from './common-functions.js'

const scratchcards = readFile('inputs/day4.txt')

// Helper functions /////////////////////////////////////////////////////////////////////////

/**
 * Extracts numbers from a string and returns them as an array of integers.
 * @param {string} string - The input string containing numbers separated by spaces.
 * @returns {number[]} - An array of integers extracted from the input string.
 */
function extractNumbersFromString(string) {
    let numbers = string.split(' ').filter(Boolean);

    // convert array to int
    for (let i = 0; i < numbers.length; i++) {
        numbers[i] = parseInt(numbers[i]);
    }

    return numbers
}

/**
 * Calculates the number of points on this card based on the winning numbers and your numbers.
 * @param {number[]} winningNumbers - The array of winning numbers.
 * @param {number[]} yourNumbers - The array of your numbers.
 * @returns {number} The number of points on this card.
 */
function getPointsOnThisCard(winningNumbers, yourNumbers) {
    let pointsOnThisCard = 0;
    for (let i = 0; i < winningNumbers.length; i++) {
        if (yourNumbers.includes(winningNumbers[i])) {
            pointsOnThisCard++;
        }
    }
    return pointsOnThisCard;
}

// Part 1 /////////////////////////////////////////////////////////////////////////

let totalPoints = 0;

scratchcards.forEach(scratchcard => {
    let numbers = scratchcard.split(':')[1];
    let [winningNumbers, yourNumbers] = numbers.split('|');

    winningNumbers = extractNumbersFromString(winningNumbers);
    yourNumbers = extractNumbersFromString(yourNumbers);

    let pointsOnThisCard = getPointsOnThisCard(winningNumbers, yourNumbers)

    if (pointsOnThisCard > 0) {
        pointsOnThisCard = Math.pow(2, pointsOnThisCard - 1);
        totalPoints += pointsOnThisCard;
    }
});

console.log('Part1 - Total points: ' + totalPoints);

// Part 2 /////////////////////////////////////////////////////////////////////////

// Initialize with 1 of each scratchcard
let countPerScratchcard = Array(scratchcards.length).fill(1);

for (let i = 0; i < scratchcards.length; i++) {

    let numbers = scratchcards[i].split(':')[1];
    let [winningNumbers, yourNumbers] = numbers.split('|');

    winningNumbers = extractNumbersFromString(winningNumbers);
    yourNumbers = extractNumbersFromString(yourNumbers);

    let pointsOnThisCard = getPointsOnThisCard(winningNumbers, yourNumbers);

    // iterate through all scratchcards and count bonus cards based on points
    for (let c = 0; c < countPerScratchcard[i]; c++) {
        for (let p = 1; p <= pointsOnThisCard; p++) {
            countPerScratchcard[i + p]++;
        }
    }
}

let totalNumberOfScratchcards = sumArray(countPerScratchcard);

console.log('Part2 - Total number of scratchcards: ' + totalNumberOfScratchcards);
