import { readFile, sumArray } from "./common-functions.js";

let workflowRatings = readFile('inputs/day19.txt');

//#region Helper functions ///////////////////////////////////////////////////////////////

class Part {
    constructor(ratings) {
        ratings = ratings.slice(1, ratings.length - 1).split(',');
        this.ratings = {};

        ratings.forEach(rating => {
            let [category, value] = rating.split('=');
            this.ratings[category.trim()] = Number(value.trim());
        });

        this.sumRating = sumArray(Object.values(this.ratings));
    }
}

class ModularPart {
    constructor(lowRating, highRating) {
        this.ratings = { x: [lowRating, highRating], m: [lowRating, highRating],
                        a: [lowRating, highRating], s: [lowRating, highRating] };
    }

    split(category, threshold) {
        let [low, high] = this.ratings[category];

        let clone = new ModularPart(0, 0);
        for (let key in this.ratings) {
            if (key != category) {
                clone.ratings[key] = this.ratings[key];
            }
            else {
                clone.ratings[key] = [threshold + 1, high];
            }
        }
        this.ratings[category] = [low, threshold];
        return clone;
    }

    getSumRating() {
        // multiply the range of each category
        let result = 1;
        for (let key in this.ratings) {
            result *= this.ratings[key][1] - this.ratings[key][0] + 1;
        }
        return result;
    }

}

class Rule {
    constructor(ruleString) {
        this.parseRuleString(ruleString);
    }

    // generate string representation of rule
    toString() {
        if (this.type == 'gt') {
            return this.category + '>' + this.value;
        }
        else {
            return this.category + '<' + this.value;
        }
    }

    parseRuleString(ruleString) {
        if (ruleString.indexOf('>') != -1) {
            let [left, right] = ruleString.split('>');
            this.category = left;
            this.value =  parseInt(right);
            this.type = 'gt';
        }
        else {
            let [left, right] = ruleString.split('<');
            this.category = left;
            this.value = parseInt(right);
            this.type = 'lt';
        }
    }

    applyRules(part) {
        // check if part is ModularPart
        if (part.constructor.name == 'ModularPart') {
            switch (this.type) {
                case 'gt':
                    if (part.ratings[this.category][1] < this.value) {
                        return [false, false];
                    }
                    else if (part.ratings[this.category][0] > this.value) {
                        return [true, part];
                    }
                    else {
                        let partTrue = part.split(this.category, this.value);
                        return [partTrue, part];
                    }
                case 'lt':
                    if (part.ratings[this.category][0] > this.value) {
                        return [false, false];
                    }
                    else if (part.ratings[this.category][1] < this.value) {
                        return [true, part];
                    }
                    else {
                        let partFalse = part.split(this.category, this.value - 1);
                        return [part, partFalse];
                    }
            }
        }
        else {
            switch (this.type) {
                case 'gt':
                    return part.ratings[this.category] > this.value;
                case 'lt':
                    return part.ratings[this.category] < this.value;
            }
        }
    }
}

class Workflow {
    constructor(name, rulesString) {
        this.name = name;
        this.rules = this.parseRules(rulesString.replace('}', ''));
    }

    parseRules(rulesString) {
        let rules = [];
        let ruleStrings = rulesString.split(',');

        ruleStrings.forEach(ruleString => {
            if (ruleString.indexOf(':') != -1) {
                let [rule, destination] = ruleString.split(':');
                rules.push([new Rule(rule), destination]);
            }
            else {
                rules.push([true, ruleString])
            }
        });
        return rules;
    }

    applyRules(part) {
        for (let i = 0; i < this.rules.length; i++) {
            let rule = this.rules[i];
            if (rule[0] == true) {
                return rule[1];
            }
            else {
                let result = rule[0].applyRules(part);
                if (result == true) {
                    return rule[1];
                }
                else if (result == false) {
                    continue;
                }
                else {
                    return result;
                }
            }
        }
    }

    applyRulesRange(partRange) {

        // console.log("Applying rules " +  this.rules + " to partRange: " + JSON.stringify(partRange.ratings) + "\n");
        // console.log("Sum of ratings: " + partRange.getSumRating() + "\n");

        let results = [];
        let partTrue;
        for (let i = 0; i < this.rules.length; i++) {
            let rule = this.rules[i];
            if (rule[0] == true) {
                results.push([rule[1], partRange]);
            }
            else {
                [partTrue, partRange] = rule[0].applyRules(partRange);
                if (partTrue == true) {
                    results.push([rule[1], partRange]);
                }
                else if (partTrue != false) {
                    results.push([rule[1], partTrue]);
                }
                else {
                    break;  // no more parts to work with
                }
            }
        }

        // console.log("Results: " + JSON.stringify(results) + "\n");
        return results;
    }

}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let workflows = {};
let parts = [];
let totalRating = 0;

for (let i = 0; i < workflowRatings.length; i++) {
    if (workflowRatings[i] == '') {
        break;
    }
    let workflow = new Workflow(...workflowRatings[i].split('{'));
    workflows[workflow.name] = workflow;
}

for (let i = Object.keys(workflows).length + 1; i < workflowRatings.length; i++) {
    parts.push(new Part(workflowRatings[i]));
}

for (let i = 0; i < parts.length; i++) {
    let workflow = workflows['in'];
    let part = parts[i];
    while (true) {
        let result = workflow.applyRules(part)
        if (result == 'A') {
            totalRating += part.sumRating;
            break;
        }
        else if (result == 'R') {
            break;
        }
        else {
            workflow = workflows[result];
        }
    }
}

console.log("Part 1: The sum of all ratings is: " + totalRating);
// 383682

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

workflows = {};

for (let i = 0; i < workflowRatings.length; i++) {
    if (workflowRatings[i] == '') {
        break;
    }
    let workflow = new Workflow(...workflowRatings[i].split('{'));
    workflows[workflow.name] = workflow;
}

let acceptedCombinations = 0;

let partRange = new ModularPart(1, 4000);

let activeWorkflows = [['in', partRange]];
let finishedWorkflows = [];

while (activeWorkflows.length > 0) {
    let [wf_id, partRange] = activeWorkflows.shift();

    if (wf_id == 'R') {
        continue;
    }
    else if (wf_id == 'A') {
        finishedWorkflows.push(partRange);
        if (partRange != true) {
            acceptedCombinations += partRange.getSumRating();
        }
        // acceptedCombinations += partRange.ratings.x[1] - partRange.ratings.x[0] + 1;
        continue;
    }

    //extend activeWorkflows with new workflows
    activeWorkflows = activeWorkflows.concat(workflows[wf_id].applyRulesRange(partRange));

}

console.log("Part 2: Number of accepted combinations: " + acceptedCombinations);
//  117954800808317
//

//#endregion
