import { readFile, getMaxOfArray } from "./common-functions.js";
import fs from 'fs';

let pipeMap = readFile('inputs/day10.txt');

// extend the map by an empty buffer row/col

// add a column filled with '.' at the beginning and end of the pipeMap
for (let i = 0; i < pipeMap.length; i++) {
    pipeMap[i] = '.' + pipeMap[i] + '.';
}

// add a row filled with '.' at the end and beginning of the pipeMap
pipeMap.unshift(new Array(pipeMap[0].length).fill('.'));
pipeMap.push(new Array(pipeMap[0].length).fill('.'));


// find starting location 'S' in the pipe Map
let startCol, startRow;
//iterate through pipeMap and find character 'S'
for (let i = 0; i < pipeMap.length; i++) {
    startCol = pipeMap[i].indexOf('S')
    if (startCol != -1) {
        startRow = i;
        break;
    }
}

// Helper functions ///////////////////////////////////////////////////////////////

function findConnectedPipes(pipeMap, row, col) {
    let connectedPipes = [];
    if (pipeMap[row][col] == 'J' || pipeMap[row][col] == 'L' || pipeMap[row][col] == '|') {
        if (pipeMap[row - 1][col] == '7' || pipeMap[row - 1][col] == 'F' || pipeMap[row - 1][col] == '|' ) {
            connectedPipes.push([row - 1, col]);
        }
    }
    if (pipeMap[row][col] == '7' || pipeMap[row][col] == 'F' || pipeMap[row][col] == '|') {
        if (pipeMap[row + 1][col] == 'L' || pipeMap[row + 1][col] == 'J' || pipeMap[row + 1][col] == '|' ) {
            connectedPipes.push([row + 1, col]);
        }
    }
    if (pipeMap[row][col] == '7' || pipeMap[row][col] == 'J' || pipeMap[row][col] == '-') {
        if (pipeMap[row][col - 1] == 'F' || pipeMap[row][col - 1] == 'L' || pipeMap[row][col - 1] == '-' ) {
            connectedPipes.push([row, col - 1]);
        }
    }
    if (pipeMap[row][col] == 'F' || pipeMap[row][col] == 'L' || pipeMap[row][col] == '-') {
        if (pipeMap[row][col + 1] == '7' || pipeMap[row][col + 1] == 'J' || pipeMap[row][col + 1] == '-' ) {
            connectedPipes.push([row, col + 1]);
        }
    }

    if (pipeMap[row][col] == 'S') {
        if (pipeMap[row - 1][col] == '7' || pipeMap[row - 1][col] == 'F' || pipeMap[row - 1][col] == '|' ) {
            connectedPipes.push([row - 1, col]);
        }
        if (pipeMap[row + 1][col] == 'L' || pipeMap[row + 1][col] == 'J' || pipeMap[row + 1][col] == '|' ) {
            connectedPipes.push([row + 1, col]);
        }
        if (pipeMap[row][col - 1] == 'F' || pipeMap[row][col - 1] == 'L' || pipeMap[row][col - 1] == '-' ) {
            connectedPipes.push([row, col - 1]);
        }
        if (pipeMap[row][col + 1] == '7' || pipeMap[row][col + 1] == 'J' || pipeMap[row][col + 1] == '-' ) {
            connectedPipes.push([row, col + 1]);
        }
    }
    return connectedPipes;
}

function writeDistanceMap(distanceMap) {
    for (let i = 0; i < distanceMap.length; i++)
    {
        for (let j = 0; j < distanceMap[0].length; j++) {
            if (distanceMap[i][j] >= 0) {
                distanceMap[i][j] = distanceMap[i][j] % 10;
            }
            else {
                distanceMap[i][j] = '.';
            }
        }
    }

    let textOutput = '';
    for (let i = 1; i < distanceMap.length - 1; i++) {
        // cut away the buffer row/col before writing
        textOutput += distanceMap[i].slice(1, -1).join('') + '\r\n';
    }

    fs.writeFile('out//distances.txt', textOutput, (err) => {
        if (err) throw err;
    });
}

function writeRegionsMap(regionMap) {
    let textOutput = '';
    for (let i = 1; i < regionMap.length - 1; i++) {
        // cut away the buffer row/col before writing
        textOutput += regionMap[i].slice(1, -1).join('') + '\r\n';
    }

    fs.writeFile('out//regions.txt', textOutput, (err) => {
        if (err) throw err;
    });
}

//#region Part 1 /////////////////////////////////////////////////////////////////////////

// create an array of the size of pipeMap with all fill values
const fillValue = -99;
let distanceMap = Array(pipeMap.length).fill(fillValue).map(() => Array(pipeMap[0].length).fill(fillValue));
distanceMap[startRow][startCol] = 0;

let connectedPipes = [[startRow, startCol]];

while (connectedPipes.length > 0) {
    let currentPipe = connectedPipes.shift();
    let newPipes = findConnectedPipes(pipeMap, currentPipe[0], currentPipe[1]);
    newPipes.forEach(pipe => {
        if (distanceMap[pipe[0]][pipe[1]] == fillValue || distanceMap[pipe[0]][pipe[1]] > distanceMap[currentPipe[0]][currentPipe[1]]) {
            distanceMap[pipe[0]][pipe[1]] = distanceMap[currentPipe[0]][currentPipe[1]] + 1;
            connectedPipes.push(pipe);
        }
    });
}

// get the maximum of the array of arrays distanceMap
let max = getMaxOfArray(distanceMap);

console.log("Part 1: The maximum number of steps is: " + max);

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

// Create an array of the size of pipeMap with all 0s for the regions; Just for visualization
let regionMap = Array(pipeMap.length).fill(0).map(() => Array(pipeMap[0].length).fill(0));

let insideFlag = false;
let potentialCrossingUp = false;
let potentialCrossingDown = false;
let insideCount = 0;

for (let i = 1; i < distanceMap.length - 1; i++) {
    insideFlag = false;
    potentialCrossingUp = false;
    potentialCrossingDown = false;
    for (let j = 1; j < distanceMap[0].length; j++) {

        if (distanceMap[i][j] >= 0) {
            // check for direct crossing
            if (Math.abs(distanceMap[i][j] - distanceMap[i+1][j]) == 1 && Math.abs(distanceMap[i][j] - distanceMap[i-1][j]) == 1) {
                insideFlag = !insideFlag;
                potentialCrossingUp = false;
                potentialCrossingDown = false;
                continue;
            }

            if (potentialCrossingDown)
            {
                if (Math.abs(distanceMap[i][j] - distanceMap[i+1][j]) == 1) {
                    insideFlag = !insideFlag;
                    potentialCrossingDown = false;
                }
                else if (Math.abs(distanceMap[i][j] - distanceMap[i-1][j]) == 1) {
                    potentialCrossingDown = false;
                }
            }
            else if (potentialCrossingUp)
            {
                if (Math.abs(distanceMap[i][j] - distanceMap[i-1][j]) == 1) {
                    insideFlag = !insideFlag;
                    potentialCrossingUp = false;
                }
                else if (Math.abs(distanceMap[i][j] - distanceMap[i+1][j]) == 1) {
                    potentialCrossingUp = false;
                }
            }
            else {
                // check for potential future crossing along the row
                if (Math.abs(distanceMap[i][j] - distanceMap[i+1][j]) == 1) {
                    potentialCrossingUp = true;
                }
                else if (Math.abs(distanceMap[i][j] - distanceMap[i-1][j]) == 1) {
                    potentialCrossingDown = true;
                }
            }
        }
        else {
            potentialCrossingUp = false;
            potentialCrossingDown = false;
            if (insideFlag) {
                regionMap[i][j] = 1;
                insideCount++;
            }
        }
    }
}

writeDistanceMap(distanceMap);
writeRegionsMap(regionMap);

console.log("Part 2: Size of the enclosed area: " + insideCount);

//#endregion
