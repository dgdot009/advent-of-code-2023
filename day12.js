import { readFile, sumArray } from "./common-functions.js";

let damageRecords = readFile('inputs/day12.txt');

//#region Helper functions ///////////////////////////////////////////////////////////////

// generate an array where at index x a string with x times '0' is contained
let zeroArray = Array(200).fill(0).map((x, i) => '0'.repeat(i));
let onesArray = Array(200).fill(0).map((x, i) => '1'.repeat(i));

// generate RegEx objects in an Array
let regexObjArray = Array(100).fill(0).map((x, i) => new RegExp('[#?]{' + i + ',}'));

// create dictionary for stored arrangements
let arrangementDict = {};

function generateBitMask(bitMaskLength, bitMaskSegmentLengths) {
    let bitMaskArrays = [];

    // generate an array of the length of bitMaskSegmentLength
    let gaps = new Array(bitMaskSegmentLengths.length).fill(1);
    gaps[0] = 0;

    let maxGapLength = bitMaskLength - bitMaskSegmentLengths.reduce((a, b) => a + b, 0) - (bitMaskSegmentLengths.length - 2);

    while (true) {
        // generate bitMaskSegmentLength
        let bitmask = '';

        for (let i = 0; i < gaps.length; i++) {
            bitmask += '0'.repeat(gaps[i]);
            bitmask += '1'.repeat(bitMaskSegmentLengths[i]);
        }
        if (bitmask.length <= bitMaskLength) {
            bitmask += '0'.repeat(bitMaskLength - bitmask.length);
            bitMaskArrays.push(bitmask);
        }
        gaps[gaps.length - 1]++;

        // check if any gap is too long
        for (let i = gaps.length - 1; i > 0; i--) {
            if (gaps[i] > maxGapLength) {
                gaps[i] = 1;
                gaps[i - 1]++;
            }
        }
        if (gaps[0] > maxGapLength) {
            break;
        }
    }

    return bitMaskArrays;
}

function countValidBitMasks(bitMaskLength, bitMaskSegmentLengths, operationalPositions, damagePositions) {
    // let bitMaskArrays = [];

    if(bitMaskSegmentLengths.length == 0 && damagePositions.length == 0){
        return 1;
    }

    let bitMaskCount = 0;

    // generate an array of the length of bitMaskSegmentLength
    let gaps = new Array(bitMaskSegmentLengths.length).fill(1);
    gaps[0] = 0;

    let maxGapLength = bitMaskLength - bitMaskSegmentLengths.reduce((a, b) => a + b, 0) - (bitMaskSegmentLengths.length - 2);
    let maxTotalGapLength = bitMaskLength - bitMaskSegmentLengths.reduce((a, b) => a + b, 0);
    let mismatch;
    let bitmask;
    let startBit = 0;

    while (true) {
        // generate bitMaskSegmentLength
        bitmask = '';
        mismatch = false;

        if (sumArray(gaps) <= maxTotalGapLength) {
            for (let i = 0; i < gaps.length; i++) {

                startBit = bitmask.length;
                bitmask += zeroArray[gaps[i]];
                bitmask += onesArray[bitMaskSegmentLengths[i]];
                if(checkBitMaskInvalid(bitmask, startBit, damagePositions, operationalPositions)){
                    mismatch = true;

                    if(i != gaps.length - 1){
                        gaps[i]++;
                        for (let j = i+1; j < gaps.length; j++) {
                            gaps[j] = 1;
                        }
                        gaps[gaps.length - 1] = 0;
                    }
                    break;
                }
            }
            if (!mismatch && bitmask.length <= bitMaskLength) {
                startBit = bitmask.length;
                bitmask += zeroArray[bitMaskLength - bitmask.length];
                if(!checkBitMaskInvalid(bitmask, startBit, damagePositions, operationalPositions)){
                    // bitMaskArrays.push(bitmask);
                    bitMaskCount++;
                }
            }
        }
        else {
            // find the last position that can be increased by one so that the resulting total gap length is not too large
            for (let i = gaps.length - 1; i >= 0; i--) {
                if (sumArray(gaps.slice(0, i+1)) + 1 < maxTotalGapLength) {
                    gaps[i]++;
                    for (let j = i + 1; j < gaps.length; j++) {
                        gaps[j] = 1;
                    }
                    gaps[gaps.length - 1] = 0;
                    break;
                }
            }
        }
        gaps[gaps.length - 1]++;

        // check if any gap is too long
        for (let i = gaps.length - 1; i > 0; i--) {
            if (gaps[i] > maxGapLength) {
                gaps[i] = 1;
                gaps[i - 1]++;
            }
        }
        if (gaps[0] > maxGapLength) {
            break;
        }
    }

    // return bitMaskArrays;
    return bitMaskCount;
}

function checkBitMaskInvalid(bitMask, startBit, damagePositions, operationalPositions) {
    for (let i = 0; i < damagePositions.length; i++) {
        if (damagePositions[i] < startBit) {
            continue;
        }
        if (damagePositions[i] >= bitMask.length) {
            break;
        }
        if (bitMask[damagePositions[i]] != '1') {
            return true;
        }
    }
    for (let i = 0; i < operationalPositions.length; i++) {
        if (operationalPositions[i] < startBit) {
            continue;
        }
        if (operationalPositions[i] >= bitMask.length) {
            break;
        }
        if (bitMask[operationalPositions[i]] != '0') {
            return true;
        }
    }
    return false;
}

function countPossibleArrangements(damageRecord, damageCounts, useDict = true) {

    if(useDict){
        let key = damageRecord + damageCounts;
        if(key in arrangementDict){
            return arrangementDict[key];
        }
        else{
            let count = countPossibleArrangements(damageRecord, damageCounts, false);
            arrangementDict[key] = count;
            return count;
        }
    }

    let match;

    // using regex, find first sequence of only '?' and '#' in damageRecord
    if (damageCounts.length > 0) {
        match = regexObjArray[damageCounts[0]].exec(damageRecord)
        if(!match){
            return 0;
       }
    }
    else {
        // we have no more damage counts, but we might have damages left in the record,
        // which would also mean an invalid arrangement
        if(damageRecord.indexOf('#') != -1){
            return 0;
        }
        else{
            return 1;
        }
    }

    if (damageRecord.slice(0, match.index).indexOf('#') != -1) {
        // we had to skip a damage to match the next damage count
        // --> invalid
        return 0;
    }

    let arrangementCount = 0;
    if (match[0][damageCounts[0]] != '#') {
        // the current sequence is a valid damage sequence
        arrangementCount = countPossibleArrangements(damageRecord.slice(match.index + damageCounts[0] + 1), damageCounts.slice(1))
    }

    if (damageRecord.slice(0, match.index + 1).indexOf('#') == -1) {
        arrangementCount += countPossibleArrangements(damageRecord.slice(match.index + 1), damageCounts);
    }

    return  arrangementCount;
}


//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let damages, damageCounts;
let totalDamageCount = 0;

for (let i = 0; i < damageRecords.length; i++) {
    [damages, damageCounts] = damageRecords[i].split(' ');

    damageCounts = damageCounts.split(',').map(Number);

    let bitMaskArrays = generateBitMask(damages.length, damageCounts);

    let damagePositions = [];
    let operationalPositions = [];
    for (let j = 0; j < damages.length; j++) {
        if (damages[j] == '#') {
            damagePositions.push(j);
        }
        else if (damages[j] == '.') {
            operationalPositions.push(j);
        }
    }

    let validBitMasksCount = bitMaskArrays.length;
    for (let j = 0; j < bitMaskArrays.length; j++) {
        let mismatch = false;
        for (let k = 0; k < damagePositions.length; k++) {
            if (bitMaskArrays[j][damagePositions[k]] != '1') {
                mismatch = true;
                break;
            }
        }
        for (let k = 0; k < operationalPositions.length; k++) {
            if (bitMaskArrays[j][operationalPositions[k]] != '0') {
                mismatch = true;
                break;
            }
        }
        if (mismatch) {
            validBitMasksCount--;
        }
    }
    totalDamageCount += validBitMasksCount;
}

console.log("Part 1: The sum of all arrangements is: " + totalDamageCount);
// 7204

//#endregion


//#region Part 2 /////////////////////////////////////////////////////////////////////////

totalDamageCount = 0;

// console.time('Part2');

for (let i = 0; i < damageRecords.length; i++) {

    // console.log('Processing: ' + (i+1));

    [damages, damageCounts] = damageRecords[i].split(' ');

    // repeat the string damages 5 times connected by a "?""
    damages = Array(5).fill(damages).join('?')
    damageCounts = damageCounts.split(',').map(Number);
    damageCounts = Array(5).fill(damageCounts).flat();

    let arrangementCount = countPossibleArrangements(damages, damageCounts);

    totalDamageCount += arrangementCount;
}

// console.timeEnd('Part2');

console.log("Part 2: The sum of all arrangements is: " + totalDamageCount);
// 1672318386674

//#endregion
