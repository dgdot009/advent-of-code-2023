import { readFile, sumArray, getMaxOfArray } from "./common-functions.js";

let layout = readFile('inputs/day16.txt');

// create 2D-array the size of the layout
let layoutArray = [];
for (let i = 0; i < layout.length; i++) {
    layoutArray.push([]);
    for (let j = 0; j < layout[i].length; j++) {
        layoutArray[i].push(layout[i][j]);
    }
}

//#region Helper functions ///////////////////////////////////////////////////////////////

// A class for rays in the contraption
let Ray = class {
    constructor(coordinate, direction) {
        this.coordinate = coordinate;
        this.direction = direction;
    }

    toString() {
        return this.coordinate + 'x' + this.direction;
    }

    // Method to move the ray forward one step
    move(layout, energizedArray, rays, rayHistory) {
        this.coordinate[0] += this.direction[0];
        this.coordinate[1] += this.direction[1];

        if (this.coordinate[0] < 0 || this.coordinate[0] >= layout.length || this.coordinate[1] < 0 || this.coordinate[1] >= layout[0].length) {
            return false;   // ray has left the contraption
        }

        let [y, x] = this.coordinate;
        let [vy, vx] = this.direction;

        energizedArray[y][x] = 1;

        if (layout[y][x] == '.') {
            return true;    // ray is still in the contraption
        }
        else if (layout[y][x] == '|') {
            if (vx != 0) {
                let newRay = new Ray([y, x], [-1, 0])
                if (!(newRay.toString() in rayHistory)) {
                    rays.push(newRay);
                    rayHistory[newRay.toString()] = 1;
                }

                this.direction = [1, 0];
                if (this.toString() in rayHistory) {
                    return false;
                }
            }
        }
        else if (layout[y][x] == '-') {
            if (vy != 0) {
                let newRay = new Ray([y, x], [0, -1]);
                if (!(newRay.toString() in rayHistory)) {
                    rays.push(newRay);
                    rayHistory[newRay.toString()] = 1;
                }

                this.direction = [0, 1];
                if (this.toString() in rayHistory) {
                    return false;
                }
            }
        }
        else if (layout[y][x] == '\\') {
            if (vx == 1) {
                this.direction = [1, 0];
            }
            else if (vx == -1) {
                this.direction = [-1, 0];
            }
            else if (vy == 1) {
                this.direction = [0, 1];
            }
            else if (vy == -1) {
                this.direction = [0, -1];
            }
        }
        else if (layout[y][x] == '/') {
            if (vx == 1) {
                this.direction = [-1, 0];
            }
            else if (vx == -1) {
                this.direction = [1, 0];
            }
            else if (vy == 1) {
                this.direction = [0, -1];
            }
            else if (vy == -1) {
                this.direction = [0, 1];
            }
        }

        if (this.toString() in rayHistory) {
            return false;
        }
        rayHistory[this.toString()] = 1;
        return true;    // ray is still in the contraption
    }
}

function performRaySimulation(layoutArray, startingRay) {
    // create empty array the size of layoutArray to store energized state
    let energizedArray = Array(layoutArray.length).fill().map(() => Array(layoutArray[0].length).fill(0));
    let currentRays = [startingRay];

    let rayHistory = {};

    while (true) {
        for (let i = 0; i < currentRays.length; i++) {
            if (!currentRays[i].move(layoutArray, energizedArray, currentRays, rayHistory)) {
                currentRays.splice(i--, 1);
                if (currentRays.length == 0) {
                    return sumArray(energizedArray);
                }
            }
        }
    }
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

// create empty array the size of layoutArray to store energized state
let energizedArray = Array(layoutArray.length).fill().map(() => Array(layoutArray[0].length).fill(0));

let currentRays = [new Ray([0, -1], [0, 1])];
let rayHistory = {};

let energizedSum = sumArray(energizedArray);

while (true) {
    for (let i = 0; i < currentRays.length; i++) {
        if (!currentRays[i].move(layoutArray, energizedArray, currentRays, rayHistory)) {
            currentRays.splice(i, 1);
            i--;
        }
    }
    if (currentRays.length == 0) {
        break;
    }
}
energizedSum = sumArray(energizedArray);
console.log("Part 1: The sum of all command hashes is: " + energizedSum);
// 7210

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

let collectedEnergizedSums = [];

// console.time('Part 2');

for (let s=0; s<layoutArray.length; s++) {

    let startingRay = new Ray([-1, s], [1, 0]);
    collectedEnergizedSums.push(performRaySimulation(layoutArray, startingRay));
    startingRay = new Ray([s, -1], [0, 1]);
    collectedEnergizedSums.push(performRaySimulation(layoutArray, startingRay));
    startingRay = new Ray([layoutArray.length + 1, s], [-1, 0]);
    collectedEnergizedSums.push(performRaySimulation(layoutArray, startingRay));
    startingRay = new Ray([s, layoutArray[0].length+1], [0, -1]);
    collectedEnergizedSums.push(performRaySimulation(layoutArray, startingRay));

    console.log('Cycle ' + (s + 1) +  ' done.')
}

// console.timeEnd('Part 2');

console.log("Part 2: The total focusing power of the configuration is: " + getMaxOfArray(collectedEnergizedSums));
// 7673

//#endregion
