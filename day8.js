import { readFile } from "./common-functions.js";

let tmp = readFile('inputs/day8.txt');
let instructions = tmp[0];
// replace 'L' with '0' and 'R' with '1'
instructions = instructions.replace(/L/g, '0').replace(/R/g, '1').split('').map(Number);

// create hash table of all instructions
const network = new Map();
for (let i = 2; i < tmp.length; i++) {
    let location = tmp[i].split('=')[0].trim();
    let [left, right] = tmp[i].split('=')[1].split(',');
    network.set(location, [left.trim().replace('(', ''), right.trim().replace(')', '')]);
}

// Helper functions ///////////////////////////////////////////////////////////////

// find least common multiple of n numbers
function findLeastCommonMultipleOfArray(numbers) {
    let lcm = numbers[0];
    for (let i = 1; i < numbers.length; i++) {
        lcm = findLeastCommonMultiple(lcm, numbers[i]);
    }
    return lcm;
}

// find least common multiple of two numbers
function findGreatestCommonDivisor(a, b) {
    if (b == 0) {
        return a;
    }
    return findGreatestCommonDivisor(b, a % b);
}

// find least common multiple of two numbers
function findLeastCommonMultiple(a, b) {
    return (a * b) / findGreatestCommonDivisor(a, b);
}

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let currentLocation = 'AAA';
let steps = 0;
const maxSteps = instructions.length;

console.time('Part 1');

while (true) {
    currentLocation = network.get(currentLocation)[instructions[steps++ % maxSteps]];
    if (currentLocation == 'ZZZ') {
        break;
    }
}

console.timeEnd('Part 1');

console.log("Part 1: Required steps to reach ZZZ: " + steps);

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

// find all elements in network that end with an A
const startLocations = Array.from(network.keys()).filter(key => key.endsWith('A'));
let currentLocations = Array.from(network.keys()).filter(key => key.endsWith('A'));
steps = 0;

let zCrossings = [];
for (let i = 0; i < startLocations.length; i++) {
    zCrossings.push([]);
}

while (true) {

    // go through all current locations
    for (let i = 0; i < currentLocations.length; i++) {

        // get next location
        currentLocations[i] = network.get(currentLocations[i])[instructions[steps % maxSteps]];

        // check if current location is a Z crossing
        if (currentLocations[i].endsWith('Z')) {
            var newZCrossing = [steps, steps % maxSteps, currentLocations[i]];
            zCrossings[i].push(newZCrossing);
        }
    }

    // increase steps
    steps++;

    if (zCrossings.every(zCrossing => zCrossing.length > 1) ) {
        // find least common multiple of all zCrossings
        let lcm = findLeastCommonMultipleOfArray(zCrossings.map(zCrossing => zCrossing[1][0] - zCrossing[0][0]));
        steps = lcm;
        break;
    }
}

console.log("Part 2: Required steps to reach **Z for all ghosts: " + steps);

//#endregion
