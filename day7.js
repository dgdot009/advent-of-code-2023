import { readFile } from "./common-functions.js";

let handsAndBids = readFile('inputs/day7.txt');
let hands = handsAndBids.map(line => line.split(' ')[0]);
let bids = handsAndBids.map(line => line.split(' ')[1]);

// Helper functions ///////////////////////////////////////////////////////////////

const handTypes = {
    'Five of a Kind': 6,
    'Four of a Kind': 5,
    'Full House': 4,
    'Three of a Kind': 3,
    'Two Pairs': 2,
    'One Pair': 1,
    'High Card': 0
}

function determineHandType(hand) {
    // get the count of each card in the hand
    let charCount = {};
    hand.split('').forEach(char => {
        charCount[char] = (charCount[char] || 0) + 1;
    });

    // extract the values from the charCount object
    let charCountValues = Object.values(charCount).sort((a, b) => b - a);

    // check for hand type
    if (charCountValues[0] == 5) {
        return handTypes['Five of a Kind'];
    }
    else if (charCountValues[0] == 4) {
        return handTypes['Four of a Kind'];
    }
    else if (charCountValues[0] == 3 && charCountValues[1] == 2) {
        return handTypes['Full House'];
    }
    else if (charCountValues[0] == 3) {
        return handTypes['Three of a Kind'];
    }
    else if (charCountValues[0] == 2 && charCountValues[1] == 2) {
        return handTypes['Two Pairs'];
    }
    else if (charCountValues[0] == 2) {
        return handTypes['One Pair'];
    }
    else {
        return handTypes['High Card'];
    }
}

function determineHandTypeWithJokers(hand) {
    // get the count of each card in the hand
    let charCount = {};
    hand.split('').forEach(char => {
        charCount[char] = (charCount[char] || 0) + 1;
    });

    // get joker count and remove jokers from charCount
    let jokerCount = 0;
    if (charCount['J'] != undefined) {
        jokerCount = charCount['J'];
        delete charCount['J'];
    }

    // extract the values from the charCount object
    let charCountValues = Object.values(charCount).sort((a, b) => b - a);

    // check for hand type
    if (charCountValues.length <= 1)  {
        return handTypes['Five of a Kind'];
    }
    else if (charCountValues[0] == 4 || (charCountValues[0] + jokerCount) == 4) {
        return handTypes['Four of a Kind'];
    }
    else if (charCountValues.length == 2) {
        return handTypes['Full House'];
    }
    else if (charCountValues[0] == 3 || (charCountValues[0] + jokerCount) == 3) {
        return handTypes['Three of a Kind'];
    }
    else if ((charCountValues[0] == 2 && charCountValues[1] == 2) || jokerCount == 2 || (charCountValues[0] == 2 && jokerCount == 1)) {
        return handTypes['Two Pairs'];
    }
    else if (charCountValues[0] == 2 || jokerCount == 1) {
        return handTypes['One Pair'];
    }
    else {
        return handTypes['High Card'];
    }
}

/**
 * Compares two hands of cards and determines the winner based on card strengths.
 * @param {Array} handA - The first hand of cards.
 * @param {Array} handB - The second hand of cards.
 * @returns {number} - 1 if handA wins, -1 if handB wins, 0 if it's a tie.
 */
function compareHands(handA, handB) {
    for (let i = 0; i < handA.length; i++) {
        let cardA = handA[i];
        let cardB = handB[i];

        if (cardA != cardB) {
            if (cardStrengths[cardA] > cardStrengths[cardB]) {
                return 1;
            }
            else if (cardStrengths[cardA] < cardStrengths[cardB]) {
                return -1;
            }
        }
    }
    return 0;
}

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let cardStrengths = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2
}

// create a new array containing 7 arrays
let handsInType = [];
for (let i = 0; i < Object.keys(handTypes).length; i++) {
    handsInType.push([]);
}

// sort into hand types
for (let i = 0; i < hands.length; i++) {
    let hand = hands[i];
    let handType = determineHandType(hand);
    handsInType[handType].push(i);
}

// rank within hand types
for (let i = 0; i < handsInType.length; i++) {
    // sort by high card
    handsInType[i].sort((a, b) => {
        return compareHands(hands[a], hands[b]);
    });
}

// build overall ranking
let overallRanking = [];
for (let i = 0; i < handsInType.length; i++) {
    for (let j = 0; j < handsInType[i].length; j++) {
        overallRanking.push(handsInType[i][j]);
    }
}

// calc total winning
let totalWinnings = 0;
for (let i = 0; i < bids.length; i++) {
    totalWinnings += bids[overallRanking[i]] * (i + 1);
}

console.log("Part 1: Total winnings: " + totalWinnings);

//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

// set new card strengths
cardStrengths = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2,
    'J': 1
}

// create a new array containing 7 arrays
handsInType = [];
for (let i = 0; i < Object.keys(handTypes).length; i++) {
    handsInType.push([]);
}

// sort into hand types
for (let i = 0; i < hands.length; i++) {
    let hand = hands[i];
    let handType = determineHandTypeWithJokers(hand);
    handsInType[handType].push(i);
}

// rank within hand types
for (let i = 0; i < handsInType.length; i++) {
    // sort by high card
    handsInType[i].sort((a, b) => {
        return compareHands(hands[a], hands[b]);
    });
}

// build overall ranking
overallRanking = [];
for (let i = 0; i < handsInType.length; i++) {
    for (let j = 0; j < handsInType[i].length; j++) {
        overallRanking.push(handsInType[i][j]);
    }
}

// calc total winning
totalWinnings = 0;
for (let i = 0; i < bids.length; i++) {
    totalWinnings += bids[overallRanking[i]] * (i + 1);
}

console.log("Part 2: Total winnings: " + totalWinnings);

//#endregion
