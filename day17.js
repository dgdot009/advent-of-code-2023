import { readFile, sumArray, getMaxOfArray, getMinOfArray } from "./common-functions.js";

let cityLayout = readFile('inputs/day17.txt');

// create 2D-array the size of the layout
let heatLossArray = [];
for (let i = 0; i < cityLayout.length; i++) {
    heatLossArray.push([]);
    for (let j = 0; j < cityLayout[i].length; j++) {
        heatLossArray[i].push(Number(cityLayout[i][j]));
    }
}

//#region Helper functions ///////////////////////////////////////////////////////////////


function propagateHeatLoss(row, col, direction, heatLossArray, stateArray, minDist=1, maxDist) {
    let thisState = stateArray[row][col];
    let newLoss, targetState;

    switch (direction) {
        case 0:
            if (col >= heatLossArray[0].length - 1) {
                return;
            }
            newLoss = heatLossArray[row][col+1]
            targetState = stateArray[row][col+1];
            break;
        case 1:
            if (row >= heatLossArray.length - 1) {
                return;
            }
            newLoss = heatLossArray[row+1][col]
            targetState = stateArray[row+1][col];
            break;
        case 2:
            if (col <= 0) {
                return;
            }
            newLoss = heatLossArray[row][col-1]
            targetState = stateArray[row][col-1];
            break;
        case 3:
            if (row <= 0) {
                return;
            }
            newLoss = heatLossArray[row-1][col]
            targetState = stateArray[row-1][col];
            break;
        }

     for (let i = 0; i < maxDist; i++) {
        let tmp = thisState[direction][i] + newLoss;
        if (tmp < targetState[direction][i + 1]) {
            targetState[direction][i + 1] = tmp;
        }
    }
    let tmp = getMinOfArray(thisState[direction].slice(minDist - 1, maxDist)) + newLoss;
    if (direction % 2 == 0) {
        if (tmp < targetState[1][0]) {
            targetState[1][0] = tmp;
        }
        if (tmp < targetState[3][0]) {
            targetState[3][0] = tmp;
        }
    }
    else {
        if (tmp < targetState[0][0]) {
            targetState[0][0] = tmp;
        }
        if (tmp < targetState[2][0]) {
            targetState[2][0] = tmp;
        }
    }
}

//#endregion

//#region Part 1 /////////////////////////////////////////////////////////////////////////

let minDist = 1;
let maxDist = 3;

let stateArray = Array(heatLossArray.length).fill().map(() => Array(heatLossArray[0].length).fill().map(() =>
                Array(4).fill().map(() => Array(maxDist + 1).fill(Infinity))));

// initialize the top left cell of the state array
stateArray[0][0] = Array(4).fill().map(() => Array(maxDist + 1).fill(0));

let lastHeatLossTotal = 0;
let minHeatLoss = Infinity;

while (true) {
    for (let row = 0; row < heatLossArray.length; row++) {
        for (let col = 0; col < heatLossArray[0].length; col++) {
            for (let direction = 0; direction < 4; direction++) {
                propagateHeatLoss(row, col, direction, heatLossArray, stateArray, minDist, maxDist);
            }
        }
    }

    // check if we reached a steady state to break the loop
    let temp = sumArray(stateArray.map(row => row.map(a => getMinOfArray(a))))
    if (temp == lastHeatLossTotal && temp != Infinity) {
        break;
    }
    lastHeatLossTotal = temp;

    // Debug print
    if (minHeatLoss != getMinOfArray(stateArray[stateArray.length - 1][stateArray[0].length - 1].map(a => a.slice(minDist, maxDist)))) {
        minHeatLoss = getMinOfArray(stateArray[stateArray.length - 1][stateArray[0].length - 1].map(a => a.slice(minDist, maxDist)));
        console.log(minHeatLoss);
    }
}

console.log("Part 1: The minimum total heat loss is: " + minHeatLoss);
// 902


//#endregion

//#region Part 2 /////////////////////////////////////////////////////////////////////////

minDist = 4;
maxDist = 10;

stateArray = Array(heatLossArray.length).fill().map(() => Array(heatLossArray[0].length).fill().map(() =>
                Array(4).fill().map(() => Array(maxDist).fill(Infinity))));

// initialize the top left cell of the state array
for (let i = 0; i < 4; i++) {
    stateArray[0][0][i][0] = 0;
}

lastHeatLossTotal = 0;
minHeatLoss = Infinity;

while (true) {
    for (let row = 0; row < heatLossArray.length; row++) {
        for (let col = 0; col < heatLossArray[0].length; col++) {
            for (let direction = 0; direction < 4; direction++) {
                propagateHeatLoss(row, col, direction, heatLossArray, stateArray, minDist, maxDist);
            }
        }
    }

    // check if we reached a steady state to break the loop
    let temp = sumArray(stateArray.map(row => row.map(a => getMinOfArray(a))))
    if (temp == lastHeatLossTotal && temp != Infinity) {
        break;
    }
    lastHeatLossTotal = temp;

    // Debug print
    if (minHeatLoss != getMinOfArray(stateArray[stateArray.length - 1][stateArray[0].length - 1].map(a => a.slice(minDist, maxDist)))) {
        minHeatLoss = getMinOfArray(stateArray[stateArray.length - 1][stateArray[0].length - 1].map(a => a.slice(minDist, maxDist)));
        console.log(minHeatLoss);
    }
}

minHeatLoss = getMinOfArray(stateArray[stateArray.length - 1][stateArray[0].length - 1].map(a => a.slice(minDist, maxDist)));

console.log("Part 2: The minimum total heat loss is: " + minHeatLoss);
// 1073

//#endregion
