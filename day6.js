import { readFile } from "./common-functions.js";

let raceInput = readFile('inputs/day6.txt');

// Part 1 /////////////////////////////////////////////////////////////////////////

let raceTimes = raceInput[0].split(':')[1].split(' ').filter(Boolean).map(Number);
let raceDistances = raceInput[1].split(':')[1].split(' ').filter(Boolean).map(Number);

let totalNumberOfRecordsBeaten = 1;

for (let raceID = 0; raceID < raceTimes.length; raceID++) {
    let recordBeaten = 0;
    const time = raceTimes[raceID];

    for (let t = 0; t < time; t++) {
        let dist = t * (time-t);
        if (dist > raceDistances[raceID]) {
            recordBeaten++;
        }
    }
    totalNumberOfRecordsBeaten *= recordBeaten;
}

console.log("Part 1: Total number of records beaten: " + totalNumberOfRecordsBeaten);

// Part 2 /////////////////////////////////////////////////////////////////////////

const raceTime = parseInt(raceInput[0].split(':')[1].replace(/\s/g, ''));
const raceDistance = parseInt(raceInput[1].split(':')[1].replace(/\s/g, ''));

let recordBeaten = 0;
let FirstBeatingTimeFound = false;

for (let t = 0; t < raceTime; t++) {
    let dist = t * (raceTime-t);
    if (dist > raceDistance) {
        recordBeaten++;
        FirstBeatingTimeFound = true;
        }
    else if (FirstBeatingTimeFound) {
        break;
    }
}

console.log("Part 2: Total number of records beaten: " + recordBeaten);
