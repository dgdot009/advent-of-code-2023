//const path = require('path');
import * as fs from 'fs';

export function readFile(file) {
    return fs
	.readFileSync(file, 'utf8')
	.toString()
	.trim()
	.split('\r\n');
}

export function sumArray(array) {
	if (Array.isArray(array)) {
		return array.reduce((a, b) => a + sumArray(b), 0);
	} else if (typeof array === 'number') {
		return array;
	} else {
		return Object.values(array).reduce((a, b) => a + sumArray(b), 0);
	}
}

export function cloneArray(array) {
	return JSON.parse(JSON.stringify(array));
}

export function getMaxOfArray(a){
	return Math.max(...a.map(e => Array.isArray(e) ? getMaxOfArray(e) : e));
  }

  export function getMinOfArray(a){
	return Math.min(...a.map(e => Array.isArray(e) ? getMinOfArray(e) : e));
  }