import { readFile } from "./common-functions.js";

let almanac = readFile('inputs/day5.txt');

// Helper functions //////////////////////////////////////////////

function parseMapping(mappingString) {
    let [destination, source, length] = mappingString.split(' ').map(Number);
    return [destination, source, length];
}

function getOverlap(inStart, inStop, sourceStart, sourceStop) {
    // get the range of values that are in both ranges
    let overlapStart = Math.max(inStart, sourceStart);
    let overlapStop = Math.min(inStop, sourceStop - 1);
    if (overlapStart > overlapStop) {
        return [NaN, NaN];
    }
    return [overlapStart, overlapStop];
}

function splitSeedRange(seedRanges, index, overlapStart, overlapStop, mapOffset) {

    let [rangeStart, rangeStop] = seedRanges[index];
    let newSeedRange = [overlapStart + mapOffset, overlapStop + mapOffset];
    seedRanges.splice(index, 1);
    if (overlapStart > rangeStart) {
        seedRanges.push([rangeStart, overlapStart - 1]);
    }
    if (overlapStop < rangeStop) {
        seedRanges.push([overlapStop + 1, rangeStop]);
    }
    return newSeedRange;
}

// Part 1 /////////////////////////////////////////////////////////////////////////

// read seed values and convert to numbers
let seeds = almanac[0].split(':')[1].split(' ').filter(Boolean).map(Number);

// go through almanac line by line
for (let i = 1; i < almanac.length; i++) {
    let line = almanac[i];

    // check if new region starts
    if (line.includes('map')) {
        // read all mappings for this region
        let mappings = [];
        line = almanac[++i];
        do {
            mappings.push(parseMapping(line));
            line = almanac[++i];

        } while (line != '' && line != undefined);

        // now we have all mappings for this region
        // go through all seeds
        for (let s = 0; s < seeds.length; s++) {

            // go through all mappings
            for (let k = 0; k < mappings.length; k++) {
                let [destination, source, length] = mappings[k];

                // check if seed is in source
                if (seeds[s] >= source && seeds[s] < source + length) {
                    // replace seed with destination
                    seeds[s] = destination + (seeds[s] - source);
                    break;
                }
            }
        }
    }
}

console.log(seeds);
console.log('Part1 - Lowest location number: ' + Math.min(...seeds));

// Part 2 /////////////////////////////////////////////////////////////////////////

let seedRanges = [];
seeds = almanac[0].split(':')[1].split(' ').filter(Boolean).map(Number);

// go through seeds and always pick two values
for (let i = 0; i < seeds.length; i += 2) {
    seedRanges.push([seeds[i], seeds[i] + seeds[i + 1] - 1]);
}

// go through almanac line by line
for (let i = 1; i < almanac.length; i++) {
    let line = almanac[i];

    // check if new region starts
    if (line.includes('map')) {
        // read all mappings for this region
        let mappings = [];
        line = almanac[++i];
        do {
            mappings.push(parseMapping(line));
            line = almanac[++i];

        } while (line != '' && line != undefined);

        let newSeedRanges = [];

        // go through all mappings
        for (let k = 0; k < mappings.length; k++) {

            let [destination, source, length] = mappings[k];

            // go through all seed ranges
            for (let s = 0; s < seedRanges.length; s++){

                let inStart = seedRanges[s][0];
                let inStop = seedRanges[s][1];

                let [overlapStart, overlapStop] = getOverlap(inStart, inStop, source, source + length);

                if (isNaN(overlapStart) || isNaN(overlapStop)) {
                    continue;
                }

                let shift = destination - source;
                newSeedRanges.push(splitSeedRange(seedRanges, s, overlapStart, overlapStop, shift));
                s--;
            }
        }
        seedRanges = seedRanges.concat(newSeedRanges);
    }
}

//get the minimum value from all seed ranges
let mappedSeedRanges = seedRanges.map(range => range[0]);
console.log('Part2 - Lowest location number: ' + Math.min(...mappedSeedRanges));
