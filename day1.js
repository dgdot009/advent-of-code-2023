import { readFile } from "./common-functions.js";

let input = readFile('inputs/day1.txt')

// Part 1 //////////////////////////////////////////////

let sumPart1 = 0;

// iterate over all lines in array input
for (let i = 0; i < input.length; i++)
{
    // find digits in each line and sum them
    let digits = input[i].match(/\d/g)
    sumPart1 += 10 * parseInt(digits[0]) + parseInt(digits[digits.length - 1])
}

console.log('Total sum in part 1: ' + sumPart1);


// Part 2 //////////////////////////////////////////////

let sumPart2 = 0;

// create an array with digits 1 to 9, both written and as numbers
let digitString = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', '1', '2', '3', '4', '5', '6', '7', '8', '9']

// create a functions that takes the words one to 9 and returns the corresponding digit
function getDigit(word)
{
    return digitString.indexOf(word) % 9 + 1
}

// iterate over all lines in array input
for (let i = 0; i < input.length; i++)
{
    let regEx = new RegExp(digitString.join('|'), 'g');

    // find any of the elements in digitString in each line and sum them
    let digits = []
    while (true)
    {
        let match = regEx.exec(input[i])
        if (match == null)
            break
        digits.push(match[0])
        regEx.lastIndex = match.index + 1;

    }
    sumPart2 += 10 *parseInt(getDigit(digits[0])) + parseInt(getDigit(digits[digits.length - 1]))
}

console.log('Total sum in part 2: ' + sumPart2);

